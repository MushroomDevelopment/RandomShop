<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="login.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/fontawesome.min.css">
    <title>Login</title>
</head>

<body>
    <div class="container">
        <header class="header"></header>
        <main>
            <div id="loginWindow">
                <div id="loginWindowLeftSide">
                    <div id="buttonContainer">
                        <button id="loginWindowButton">login</button>
                        <button id="signUpWindowButton">sign up</button>
                    </div>
                </div>
                <div id="loginWindowRightSide">
                    <div id="loginHeading">
                        <h2 id="heading">Login</h2>
                    </div>
                    <div id="formContainer">
                        <form id="loginInput" action="forwarding.php" method="POST">
                            <input type="email" name="email" id="emailForm " placeholder="Email" >
                            <input type="password" name="password" id="passwordInput" placeholder="Password" >
                            <div id="inputAppend"></div>
                            <button type="submit" id="loginButton">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    </div>

    <script>
        const body = document.body
        const loginWindowButton = document.getElementById("loginWindowButton")
        const signUpWindowButton = document.getElementById("signUpWindowButton")
        const heading = document.getElementById("heading")
        const form = document.getElementById("loginInput")
        const inputAppend = document.getElementById("inputAppend")
        const loginButton = document.getElementById("loginButton")
        let formAppended = false
        let passwordInput = document.createElement("input")

        loginWindowButton.onclick = function(){
            console.log("loginWindowButton")
            heading.innerText = "Login"
            loginButton.innerText = "Login"
            loginWindowButton.style.backgroundColor = "white"
            signUpWindowButton.style.backgroundColor = "rgb(219, 219, 219)"

            if(formAppended){
                passwordInput.remove()
                formAppended = false
            }
        } 

        signUpWindowButton.onclick = function(){
            console.log("signUpWindowButton")
            heading.innerText = "Sign Up"
            loginButton.innerText = "Sign Up"
            signUpWindowButton.style.backgroundColor = "white"
            loginWindowButton.style.backgroundColor = "rgb(219, 219, 219)"

            if(!formAppended){
                passwordInput.setAttribute("placeholder", "Repeat Password")
                passwordInput.setAttribute("name", "repeatedPassword")
                passwordInput.setAttribute("type", "password")
                inputAppend.append(passwordInput)
                formAppended = true
            }
        }
        
    </script>
</body>

</html>